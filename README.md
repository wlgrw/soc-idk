# SOC IDK (Sytem On a Chip Rotary Dial)
This is a PCB designed for a rotary dial (Kiesschijf in Dutch) to be used in combination with the DE10-Lite FPGA development board. 

Together with the Rotary Dial, a 3x3 RGB led matrix was added to the PCB so that a game of tic-tac-toe can be played.

# Work in progress. 
This repository is work in progress. 

## Responsible disclosure 
Students and lecturers are encouraged to send in their experiences, problems, errors, any other observation while using this repository. Please send your feedback to one of the lecturers, by raising an issue with this repository, so we can improve this paper. Thank you.

More about responsible disclosure, see: https://en.wikipedia.org/wiki/Responsible_disclosure 

## Disclaimer
The SOC IDK repository is provided in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  
## License
The SOK ID repository is free: You can redistribute it and/or modify it under the terms of a Creative Commons Attribution-NonCommercial 4.0 International License (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.



