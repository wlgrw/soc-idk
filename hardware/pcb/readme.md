# PCB design
The idea of the PCB was to give it a retro-look using through-hole components.

## KiCad 3D render.
[![3D render in KiCad](soc-idk_20231225_200x108.png 'Full size image of 3D-render')](soc-idk_20231225.png)

*Click on image for full size version.*

## Production
At [EuroCircuits](https://www.eurocircuits.com) the prototype was previewed in the following imges: 

[![PCB Front](PCBFront_150x246.png 'PCB front view')](PCBFront.png) [![PCB Back](PCBBack_150x248.png 'PCB back view')](PCBBack.png)

*Click on image for full size version.*

### Manufacturing
These PCB's have been manufactured by [EuroCircuits](https://www.eurocircuits.com) as part of their [Educational support](https://www.eurocircuits.com/students-teachers/).

![EuroCircuits](https://www.eurocircuits.com/wp-content/uploads/eclogo_v2.png)
