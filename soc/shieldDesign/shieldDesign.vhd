--------------------------------------------------------------------
--! \file      shieldDesign.vhd
--! \date      see top of 'Version History'
--! \brief     development project for shield design of soc-idk-shield
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN AEA ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |4-1-2024   |WLGRW   |Inital version
--!        |           |        |
--!
--! # Layout DE10-Lite
--!
--! \verbatim
--!                                                  +--+
--!      DE10-Lite KEY, SW, LED, and HEX layout      |##| <= KEY0
--!                                                  +--+
--!                                                  |##| <= KEY1
--!                                                  +--+
--!
--!                                  9 8 7 6 5 4 3 2 1 0  <- Number
--!                                 +-+-+-+-+-+-+-+-+-+-+
--!    7-segment hexDisplays (HEX)  | | | | | | | | | | | <= Leds (LEDR)
--!      +---+---+---+---+---+---+  +-+-+-+-+-+-+-+-+-+-+
--!      |   |   |   |   |   |   |                     
--!      |   |   |   |   |   |   |  +-+-+-+-+-+-+-+-+-+-+
--!      |   |   |   |   |   |   |  | | | | | | | | | | |
--!      |   |   |   |   |   |   |  +-+-+-+-+-+-+-+-+-+-+
--!      |   |   |   |   |   |   |  |#|#|#|#|#|#|#|#|#|#| <= Switch (SW)
--!      +---+---+---+---+---+---+  +-+-+-+-+-+-+-+-+-+-+
--!        5   4   3   2   1   0     9 8 7 6 5 4 3 2 1 0  <- Number
--!
--! \endverbatim
--!
--------------------------------------------------------------------
LIBRARY ieee;                      -- this lib needed for STD_LOGIC
USE ieee.std_logic_1164.all;       -- the package with this info
--------------------------------------------------------------------
ENTITY shieldDesign is

   PORT(
         MAX10_CLK1_50 : IN  STD_LOGIC;                  --! 50 MHz clock on board the Cycone MAX 10 FPGA
         arduino_io0   : OUT STD_LOGIC;                  --! LED-matrix signal Y1
         arduino_io1   : OUT STD_LOGIC;                  --! LED-matrix signal Y2
         arduino_io2   : OUT STD_LOGIC;                  --! LED-matrix signal Y3
         arduino_io3   : OUT STD_LOGIC;                  --! LED-matrix signal X1R
         arduino_io4   : OUT STD_LOGIC;                  --! LED-matrix signal X1G
         arduino_io5   : OUT STD_LOGIC;                  --! LED-matrix signal X1B
         arduino_io6   : OUT STD_LOGIC;                  --! LED-matrix signal X2R
         arduino_io7   : OUT STD_LOGIC;                  --! LED-matrix signal X2G
         arduino_io8   : OUT STD_LOGIC;                  --! LED-matrix signal X2B
         arduino_io9   : OUT STD_LOGIC;                  --! LED-matrix signal X3R
         arduino_io10  : OUT STD_LOGIC;                  --! LED-matrix signal X3G
         arduino_io11  : OUT STD_LOGIC;                  --! LED-matrix signal X3B
         arduino_io12  : IN  STD_LOGIC;                  --! Rotary dial signal IDK_BLUE
         arduino_io13  : IN  STD_LOGIC;                  --! Rotary dial signal IDK_RED
         arduino_io14  : OUT STD_LOGIC;                  --! LED_A
         arduino_io15  : OUT STD_LOGIC;                  --! LED_B
         HEX0, 
         HEX1,   
         HEX2, 
         HEX3,
         HEX4,
         HEX5          : OUT STD_LOGIC_VECTOR(0 TO 7);   --! 7-segment displays HEX0 to HEX5
         KEY           : IN  STD_LOGIC_VECTOR(0 TO 1);   --! Buttons
         SW            : IN  STD_LOGIC_VECTOR(0 TO 9);   --! Switches
         LEDR          : OUT STD_LOGIC_VECTOR(0 TO 9)    --! 9 LEDs
        );

END ENTITY shieldDesign;
--------------------------------------------------------------------
ARCHITECTURE implementation OF shieldDesign IS
   
-- Empty declerative part

BEGIN

-- Empty Architecture

END ARCHITECTURE implementation;
--------------------------------------------------------------------
