http://www.pldworld.com/_altera/html/_sw/q2help/source/vhdl/vhdl_pro_ram_inferred.htm

```vhdl
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
 
ENTITY mem IS
PORT (
	clock     			: IN  STD_LOGIC;
	write_enabled		: IN  STD_LOGIC;
	address				: IN  INTEGER RANGE 0 to 404639;
	address2				: IN  INTEGER RANGE 0 to 404639;
	datain  				: IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
	dataout  			: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END mem;
 
ARCHITECTURE bhv OF mem IS
TYPE fr IS ARRAY (0 to 404639) OF std_logic_vector(7 DOWNTO 0);
SIGNAL pix : fr;
	BEGIN
		PROCESS(clock)
			BEGIN
				IF rising_edge(clock) THEN
					IF(write_enabled='1')THEN
						pix(address)<=datain;
					END IF;
				dataout<=pix(address);
				END IF;
  		END PROCESS;
END ARCHITECTURE bhv;
```