# Documentation

This is a collection of documentation relevant to the project. The documentation is in random order and uncategorised 

More information on a use case example of a rotary dial can be found here: [https://www.whizzbizz.com/nl/gpo746-t65-rotary-dial-phone-mp3-wav-player](https://www.whizzbizz.com/nl/gpo746-t65-rotary-dial-phone-mp3-wav-player)


## RotaryEncoderFootPrintCutOut.*

This is the exact dimensions of the back side of the the rotary dial used for the project. It should have an tight fit on the PCB through the hole when mounted. Later it was decided that a more general cutout was required. These files are for reference now.

 - The outline designed with Sketchup 8: [RotaryEncodeFootPrintCutOut.skp](RotaryEncodeFootPrintCutOut.skp)
 - The outline exported as vector file: [RotaryEncodeFootPrintCutOut.svg](RotaryEncodeFootPrintCutOut.svg)
